import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CalculatorUnitTest {
    @Test public final void twoNumbers() {
        StringCalculator.add("5,3");
        assertTrue(true);
    }

	@Test public final void oneNumber() {
        StringCalculator.add("5");
        assertTrue(true);
    }

	@Test public final void noNumbers() {
        assertEquals(0, StringCalculator.add(""));
    }

    @Test(expected=RuntimeException.class)
    public final void moreThan2Numbers() {
        StringCalculator.add("1,2,3");
    }
    
    @Test(expected=RuntimeException.class)
    public final void noFirstNumber() {
        StringCalculator.add(",3");
    }
    
    @Test(expected=RuntimeException.class)
    public final void nonNumberUsed() {
        StringCalculator.add("1,X");
    }    
}

